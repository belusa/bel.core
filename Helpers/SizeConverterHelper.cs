﻿using System;

public class SizeConverterHelper
{
    public static string GetSize(string parameterValue)
    {
        switch (parameterValue)
        {
            case "Extra Small":
                return "XS";
            case "Small":
                return "S";
            case "Medium":
                return "M";
            case "Large":
                return "L";
            case "Extra Large":
                return "XL";
            case "XXL":
                return "XXL";
            case "XXXL":
                return "3XL";
            case "XXXXL":
                return "4XL";
            case "XXXXXL":
                return "5XL";
            case "XXXXXXL":
                return "6XL";
        }
        return "";
    }

    public static string GetSizeInvert(string parameterValue)
    {
        switch (parameterValue)
        {
            case "XS":
                return "XS";
            case "S":
                return "S";
            case "M":
                return "M";
            case "L":
                return "L";
            case "XL":
                return "XL";
            case "XXL":
                return "XXL";
            case "3XL":
                return "XXXL";
            case "4XL":
                return "XXXXL";
            case "5XL":
                return "XXXXXL";
            case "6XL":
                return "XXXXXXL";
        }

        return "";
    }
}
