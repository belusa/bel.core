﻿using System;

public class QueryStringValidator
{
    public static bool IsQueryStringParameterInteger(string parameterValue)
    {
        if (string.IsNullOrWhiteSpace(parameterValue))
        {
            return false;
        }

        for (int i = 0; i < parameterValue.Length; i++)
        {
            if (!char.IsDigit(parameterValue[i]))
            {
                return false;
            }
        }

        return true;
    }
}
