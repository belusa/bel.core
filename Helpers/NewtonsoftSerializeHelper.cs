﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace bel.core.Helpers
{
    public class NewtonsoftSerializeHelper: DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var properties = base.CreateProperties(type, memberSerialization);

            foreach (var prop in properties)
            {
                prop.PropertyName = char.ToLower(prop.PropertyName[0]) + prop.PropertyName.Substring(1);
            }

            return properties;
        }
    }
}
