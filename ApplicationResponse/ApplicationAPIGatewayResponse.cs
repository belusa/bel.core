﻿using Amazon.Lambda.APIGatewayEvents;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace bel.core.ApplicationResponse
{
    public class ApplicationAPIGatewayResponse : APIGatewayProxyResponse
    {
        public ApplicationAPIGatewayResponse() : base()
        {
            StatusCode = StatusCodes.Status200OK;

            Body = JsonConvert.SerializeObject(new ApplicationResponse());
        }

        public ApplicationAPIGatewayResponse(ApplicationEvent applicationEvent, int statusCode = ApplicationStatusCode.Status200OK, string payload = null) : base()
        {
            StatusCode = statusCode;

            Body = JsonConvert.SerializeObject(new ApplicationResponse(applicationEvent) { Payload = payload ?? string.Empty });
        }

        public ApplicationAPIGatewayResponse(IEnumerable<ApplicationEvent> applicationEvents, int statusCode = ApplicationStatusCode.Status200OK, string payload = null)
        {
            StatusCode = statusCode;

            Body = JsonConvert.SerializeObject(new ApplicationResponse(applicationEvents) { Payload = payload ?? string.Empty });
        }

        public ApplicationAPIGatewayResponse(int pageNumber, int pageSize, int totalRecords, ApplicationEvent applicationEvent, int statusCode = ApplicationStatusCode.Status200OK, string payload = null) : base()
        {
            StatusCode = statusCode;

            Body = JsonConvert.SerializeObject(new ApplicationPagedResponse(pageNumber, pageSize, totalRecords, applicationEvent) { Payload = payload ?? string.Empty });
        }
    }

    public static class ApplicationStatusCode
    {
        public const int Status200OK = 200;

        public const int Status400BadRequest = 400;

        public const int Status500InternalServerError = 500;
        
        public const int Status404NotFound = 404;
    }
}
