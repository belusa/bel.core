﻿namespace bel.core.ApplicationResponse
{
    static public class ApplicationResponseCodes
    {
        public struct Application
        {
            public const string Error = "app.error";
            public const string ErrorDescription = "An error ocurred in the app.";

            public const string Ok = "app.ok";
            public const string OkDescription = "Ok.";

            public const string EnumValueNotSupported = "app.enum_value_not_supported";
            public const string EnumValueNotSupportedDescription = "The value must be a number defined in the enum data type.";

            public const string InvalidDateTime = "app.invalid.date_time";
            public const string InvalidDateTimeDescription = "Invalid date time format.";

            public const string UndefinedEnvironmentVariable = "app.undefined.enviroment_variable";
            public const string UndefinedEnvironmentVariableDescription = "Undefined enviroment variable: {0}.";

            public const string UndefinedSecret = "app.error.undefined_secret";
            public const string UndefinedSecretDescription = "Undefined secret {0}.";

            public const string RequiredRequestBody = "app.require_request_body";
            public const string RequiredRequestBodyDescription = "Required request body.";

            public const string InvalidRequestBody = "app.invalid_request_body";
            public const string InvalidRequestBodyDescription = "Invalid request body.";

            public const string RequiredParameter = "app.require_parameter";
            public const string RequiredParameterDescription = "Required parameter {0}.";

            public const string ErrorApiEndPoint = "app.error_{0}_server";
            public const string ErrorApiEndPointDescription = "An error ocurred using the end point {0}.";

            public const string InvalidParameterValue = "app.invalid_parameter_value";
            public const string InvalidParameterValueDescription = "Invalid parameter value.";

            public const string RequiredHeader = "app.required_header";
            public const string RequiredHeaderDescription = "Required header {0}.";

            public const string InvalidEmailAddress = "app.invalid_email_address";
            public const string InvalidEmailAddressDescription = "Invalid email address {0}.";

            public const string NoItemAvailabilityCode= "app.no_item_availability_description";
            public const string NoItemAvailabilityDescription = "No items available for partner."; 

            public const string EnumValueNotSupportedDeliveryMethod = "app.enum_value_not_supported";
            public const string EnumValueNotSupportedDeliveryMethodDescription = "The value must be a Mailgun or Hubspot.";
        }

        public struct Dynamo
        {
            public const string UndefinedUrlPaths = "undefined.url_paths";
            public const string UndefinedUrlPathsDescription = "Undefined URL_PATHS.";

            public const string UndefinedHomePageImagePath = "undefined.home_page_image_path";
            public const string UndefinedHomePageImagePathDescription = "Undefined HOMEPAGE_IMAGE_PATH.";

            public const string UndefinedDetailImagePath = "undefined.detail_image_path";
            public const string UndefinedDetailImagePathDescription = "Undefined DETAIL_IMAGE_PATH.";

            public const string UndefinedColorImagePath = "undefined.color_image_path";
            public const string UndefinedColorImagePathDescription = "Undefined COLOR_IMAGE_PATH.";

            public const string UndefinedItem = "undefined.item";
            public const string UndefinedItemDescription = "Undefined item with key {0} in table {1}.";
        }

        public struct SQS
        {
            public const string ErrorSendingMsgToQueue = "error.sending_message_to_queue";
            public const string ErrorSendingMsgToQueueDescription = "Error sending message to the queue.";

            public const string ErrorDeletingMsgFromQueue = "error.deleting_message_from_queue";
            public const string ErrorDeletingMsgFromQueueDescription = "Error deleting message from queue {0}.";

            public const string ErrorMsgWithoutAttribute = "error.msg_without_attribute";
            public const string ErrorMsgWithoutAttributeDescription = "Error message without {0} attribute.";

            public const string ErrorMsgWithoutBody = "error.msg_without_body";
            public const string ErrorMsgWithoutBodyDescription = "Error, message without body.";

            public const string RequiredMessageAttribute = "sqs.required_message_attibute";
            public const string RequiredMessageAttributeDescription = "Required the message attribute {0}.";

            public const string ErrorSendingBatchMsgToQueue = "error.sending_batch_message_to_queue";
            public const string ErrorSendingBatchMsgToQueueDescription = "Error sending batch message to the queue {0}.";

            public const string RequiredQueueUrlOrEnvironmentVariable = "error.required_queue_url_or_enviroment_variable";
            public const string RequiredQueueUrlOrEnvironmentVariableDescription = "Required QueueUrl or EnvironmentVariable properties.";
            
        }

        public struct ShipStation
        {
            public const string ResourceNotfound = "error.resource_not_found";
            public const string ResourceNotfoundDescription = "Resource not found.";

            public const string OrderNotfound = "error.resource_not_found";
            public const string OrderNotfoundDescription = "Error, the order by {0} was not found.";

            public const string ShipStationOrderSkipped = "app.error.shipstation_order_skipped";
            public const string ShipStationOrderSkippedDescription = "The ShipStation order was skipped.";
        }

        public struct Stripe
        {
            // NOT CHANGE THIS VALUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // Represent a Stripe error code.
            public const string ResourceMissing = "resource_missing";
            // NOT CHANGE THIS VALUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            public const string StripeErrorDeletingPaymentMethod = "app.stripe.error_deleting_payment_method";
            public const string StripeErrorDeletingPaymentMethodDescription = "Error deletion payment method in Stripe.";
        }

        #region BEL-AMP
        public struct Amp
        {
            public const string ErrorNullContent = "response.null";
            public const string ErrorNullContentDescription = "Null response.";

            public const string ErrorNotCompleted = "response.not.completed";
            public const string ErrorNotCompletedDescription = "Response not completed.";

            public const string ErrorNotOk = "response.not.ok";
            public const string ErrorNotOkDescription = "Response not OK";

            public const string ErrorUnauthorized = "response.unauthorized";
            public const string ErrorUnauthorizedDescription = "Unauthorized";

            public const string ErrorNotContent = "response.not.content";
            public const string ErrorNotContentDescription = "No content response.";

            public const string ErrorWrongObject = "response.wrong.objet";
            public const string ErrorWrongObjectDescription = "Wrong object";

            public const string ErrorHasError = "response.haserror";
            public const string ErrorHasErrorDescription = "Has error";

            public const string UndefinedPartners = "app.undefined.partners";
            public const string UndefinedPartnersDescription = "Undefined partners.";

            public const string UnSupportedShop = "app.unsuppored_shop";
            public const string UnSupportedShopDescription = "Unsupported shop to {0}.";

            public const string InvalidSignature = "app.invalid_signature";
            public const string InvalidSignatureDescription = "Invalid signature.";

            public const string TestMode = "app.test_mode";
            public const string TestModeDescription = "Test mode detected.";

            public const string InvalidSKU = "app.unsuppored_sku";
            public const string InvalidSKUDescription = "Unsupported SKU to {0}.";

            public const string DesignNotFound = "app.unsuppored_design";
            public const string DesignNotFoundDescription = "Error, design not found to {0} on S3.";

            public const string UnSupportedImprintLocation = "app.unsuppored_imprint_location";
            public const string UnSupportedImprintLocationDescription = "Unsupported imprint location to {0}.";

            public const string UnSupportedPrintMethod = "app.unsuppored_print_method";
            public const string UnSupportedPrintMethodDescription = "Unsupported print method to {0}.";

            public const string UnSupportedVariant = "app.unsuppored_variant";
            public const string UnSupportedVariantDescription = "Unsupported variant to {0}.";

            public const string UnSupportedItem = "app.unsuppored_item";
            public const string UnSupportedItemDescription = "Unsupported item to {0}.";

            public const string UnSupportedSize = "app.unsuppored_size";
            public const string UnSupportedSizeDescription = "Unsupported size to {0}.";

            public const string ErrorItemQuantity = "app.error_item_quantity";
            public const string ErrorItemQuantityDescription = "Error in the item quantity, value equal to {0}.";

            public const string ErrorItemLineId = "app.error_item_line_id";
            public const string ErrorItemLineIdDescription = "Error in the item line id, value equal to {0}.";

            public const string NotFoundItemDetail = "app.not_found_item_detail";
            public const string NotFoundItemDetailDescription = "Not found item detail for brand({0}), item({1}), variant({2}) and size({3}).";

            public const string OrderNotFound = "app.order_not_found";
            public const string OrderNotFoundDescription = "Order not found by order number {0} and brand {1}.";

            public const string OrderNotFoundByProducerVendor = "app.order_not_found_by_producer_vendor";
            public const string OrderNotFoundByProducerVendorDescription = "Order not found by vendor order id {0}.";

            public const string NotFoundStepMapping = "app.not_found_step_mapping";
            public const string NotFoundStepMappingDescription = "Not found step mapping by vendor id {0} and step name {1}.";

            public const string NoItemAvailability = "app.no.item_availability";
            public const string NoItemAvailabilityDescription = "No avialable items for partner id {0}.";

            public const string ErrorOrderTagNotRemovedInShipStation = "app.error.order_tag_not_remove_in_shipstation";
            public const string ErrorOrderTagNotRemovedInShipStationDescription = "Error order tag not remove in shipstation.";
        }

        public struct AmpUiFilter
        {
            public const string NotCreated = "filter.not_created";
            public const string NotCreatedDescription = "The filter was not created.";

            public const string NotDeleted = "filter.not_deleted";
            public const string NotDeletedDescription = "The filter was not deleted.";

            public const string NotUpdated = "filter.not_updated";
            public const string NotUpdatedDescription = "The filter was not updated.";
        }

        public struct AmpProductionTemplate
        {
            public const string NotFound = "production_template.not_found";
            public const string NotFoundDescription = "Production template was not found by itemId = {0} and printMethodId = {1}.";
        }

        public struct AmpDesign
        {
            public const string NotUpdated = "design.not_updated";
            public const string NotUpdatedDescription = "The design with name {0}, was not updated.";
        }

        public struct AmpProductionVendorMapping
        {
            public const string NotFound = "production_vendor_mapping.not_found";
            public const string NotFoundDescription = "Production vendor mapping was not found by item = {0}, size = {1} and color = {2}.";
        }

        public struct AmpScalablePress
        {
            public const string QuoteIssue = "error.quote_issues";
            public const string QuoteIssueDescription = "One or more quotes have issues.";

            public const string OrderNotPlaced = "error.order_not_placed";
            public const string OrderNotPlacedDescription = "Error, the order with id {0} and token {1} was not placed.";
        }

        public struct AmpBrand
        {
            public const string NotFound = "brand.not_found";
            public const string NotFoundDescription = "Brand was not found by id = {0}.";
        }
        #endregion

        #region Bel-Integration-API
        public struct BelIntegrationAPI
        {
            public const string NoItemAvailability = "app.no.item_availability";
            public const string NoItemAvailabilityDescription = "No avialable items for partner id {0}.";
            public const string LineDetailsEmpty = "No Lines found for order: {0}.";
        }
        #endregion
    }
}
