﻿using System;
using System.Collections.Generic;

namespace bel.core.ApplicationResponse
{
    public class ApplicationPagedResponse : ApplicationResponse
    {
        public const int MaxPageSize = 100;

        public int? PageNumber { get; set; }

        public int? PageSize { get; set; }

        public int? TotalPages { get; set; }

        public int? TotalRecords { get; set; }

        private void ApplicationPagedResponseFactory(int pageNumber, int pageSize, int totalRecords)
        {
            PageNumber = pageNumber < 1 ? 1 : pageNumber;

            PageSize = pageSize < 1 || pageSize > MaxPageSize ? MaxPageSize : pageSize;

            TotalRecords = totalRecords;

            double totalPages = (double)TotalRecords.Value / PageSize.Value;

            TotalPages = Convert.ToInt32(Math.Ceiling(totalPages));
        }

        public ApplicationPagedResponse() : base()
        {
            PageNumber = null;
            PageSize = null;
            TotalPages = null;
            TotalRecords = null;
        }

        public ApplicationPagedResponse(ApplicationEvent applicationEvent) : base(applicationEvent)
        {
            PageNumber = null;
            PageSize = null;
            TotalPages = null;
            TotalRecords = null;
        }

        public ApplicationPagedResponse(
            int pageNumber,
            int pageSize,
            int totalRecords,
            IDictionary<string, string> messages,
            string eventCode,
            string eventType) : base(messages, eventCode, eventType)
        {
            ApplicationPagedResponseFactory(pageNumber, pageSize, totalRecords);
        }

        public ApplicationPagedResponse(
            int pageNumber,
            int pageSize,
            int totalRecords,
            ApplicationEvent applicationEvent) : base(applicationEvent)
        {
            ApplicationPagedResponseFactory(pageNumber, pageSize, totalRecords);
        }

        public ApplicationPagedResponse(
            int pageNumber,
            int pageSize,
            int totalRecords,
            IEnumerable<ApplicationEvent> applicationEvents) : base(applicationEvents)
        {
            ApplicationPagedResponseFactory(pageNumber, pageSize, totalRecords);
        }
    }
}
