﻿using Newtonsoft.Json;
using System;

namespace bel.core.ApplicationResponse
{
    public class BelApplicationException : Exception
    {
        //public BelApplicationException(string message)
        //    : base($"Error calling the application. error message: {message}")
        //{
        //}

        public BelApplicationException(BelApplicationMessageException message)
            : base(JsonConvert.SerializeObject(message))
        {
        }
    }

    public class BelApplicationMessageException
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}
