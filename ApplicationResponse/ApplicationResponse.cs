﻿using System.Collections.Generic;
using System.Linq;

namespace bel.core.ApplicationResponse
{
    public static class EventTypesConstants
    {
        public const string ErrorEvent = "Error";
        public const string InfoEvent = "Info";
    }

    public class ApplicationResponse
    {
        public ApplicationResponse() { }

        public ApplicationResponse(IDictionary<string, string> messages, string eventCode, string eventType) : this()
        {
            DigestErrors(messages, eventCode, eventType);
        }

        public ApplicationResponse(ApplicationEvent applicationEvent)
        {
            ApplicationEvents.Add(applicationEvent);
        }

        public ApplicationResponse(IEnumerable<ApplicationEvent> applicationEvents)
        {
            ApplicationEvents.AddRange(applicationEvents);
        }

        public List<ApplicationEvent> ApplicationEvents { get; set; } = new List<ApplicationEvent>();

        public object Payload { get; set; }

        public bool HasErrors
        {
            get
            {
                return ApplicationEvents.Any(e => e.Type == EventTypesConstants.ErrorEvent);
            }
        }

        private void DigestErrors(IDictionary<string, string> errors, string errorCode, string eventType)
        {
            foreach (var error in errors)
            {
                switch (eventType)
                {
                    case EventTypesConstants.InfoEvent:
                        ApplicationEvents.Add(new ApplicationEventInfo(errorCode, error.Value, error.Key));
                        break;

                    case EventTypesConstants.ErrorEvent:
                    default:
                        ApplicationEvents.Add(new ApplicationEventError(errorCode, error.Value, error.Key));
                        break;
                }
            }
        }
    }

    public class ApplicationEvent
    {
        public string Type { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

        public string Parameter { get; set; }

        public ApplicationEvent()
        {
            Type = string.Empty;

            Code = string.Empty;

            Message = string.Empty;

            Parameter = string.Empty;
        }
    }

    public class ApplicationEventError : ApplicationEvent
    {
        public ApplicationEventError()
        {
            Type = EventTypesConstants.ErrorEvent;
        }
        public ApplicationEventError(string code, string message, string parameter = null) : this()
        {
            Code = code;
            Message = message;
            Parameter = parameter ?? string.Empty;
        }
    }

    public class ApplicationEventInfo : ApplicationEvent
    {
        public ApplicationEventInfo()
        {
            Type = EventTypesConstants.InfoEvent;
        }
        public ApplicationEventInfo(string code, string message, string parameter = null) : this()
        {
            Code = code;
            Message = message;
            Parameter = parameter ?? string.Empty;
        }
    }
}
