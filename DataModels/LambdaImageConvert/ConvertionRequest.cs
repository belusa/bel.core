﻿namespace bel.core.DataModels.LambdaImageConvert
{
    public class ConvertionRequest
    {
        public string key { get; set; }
        public string bucket { get; set; }
    }
}
