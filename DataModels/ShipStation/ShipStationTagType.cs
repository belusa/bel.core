﻿namespace bel.core.DataModels.ShipStation
{
    public enum ShipStationTagType
    {
        /// <summary>
        /// The BEL item.
        /// </summary>
        BelItem = 105188,

        /// <summary>
        /// The BEL imported.
        /// </summary>
        BelImported = 105061,

        /// <summary>
        /// The bel processed.
        /// </summary>
        BelProcessed = 107391,

        /// <summary>
        /// The bel exception.
        /// </summary>
        BelException = 107816,

        /// <summary>
        /// The mixed.
        /// </summary>
        Mixed = 113316,

        /// <summary>
        /// The miami.
        /// </summary>
        Miami = 113315,

        /// <summary>
        /// The manual.
        /// </summary>
        Manual = 113319,

        /// <summary>
        /// The custom.
        /// </summary>
        Custom = 113317,

        /// <summary>
        /// The clayton.
        /// </summary>
        Clayton = 113314,

        /// <summary>
        /// The automated.
        /// </summary>
        Automated = 113318,

        OutsourceInProgress = 126395,

        OutsourceFailed = 126396,

        VendorReceived = 126397
    }
}
