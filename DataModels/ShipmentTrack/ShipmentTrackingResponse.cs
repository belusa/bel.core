﻿using System;

namespace bel.core.DataModels.ShipmentTrack
{
    public class ShipmentTrackingResponse
    {
        public string ShipmentStatus { get; set; }

        public DateTime LastUpdatedDate { get; set; }


        public ShipmentTrackingResponse()
        {
            ShipmentStatus = "";

            LastUpdatedDate = DateTime.MinValue;
        }
    }
}
