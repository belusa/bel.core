﻿namespace bel.core.DataModels.ShipmentTrack
{
    public class ShipmentTrackingRequest
    {
        public string TrackingNumber { get; set; }

        public string Carrier { get; set; }
    }
}
