﻿namespace bel.core.DataModels.Partner
{
    public class OrderCheck
    {
        public string Partner { get; set; }
        
        public string OrderId { get; set; }
        
        public OrderCheck()
        {
        }
    }
}
