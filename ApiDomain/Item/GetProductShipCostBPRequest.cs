﻿namespace bel.core.ApiDomain.Item
{
    public class GetProductShipCostBPRequest
    {
        public string ProductCode { get; set; }
        public string ProductColor { get; set; }
        public string ProductQuantity { get; set; }
        public string PrintOption { get; set; }
        public string StateCode { get; set; }
        public string CustomerId { get; set; }
    }
}
