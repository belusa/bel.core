﻿namespace bel.core.ApiDomain.Sales
{
    public class CAMCampaigReturningRequest
    {
        public string CustomerID { get; set; }

        public string Email { get; set; }
    }
}
