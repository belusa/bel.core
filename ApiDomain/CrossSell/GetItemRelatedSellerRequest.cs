﻿namespace bel.core.ApiDomain.CrossSell
{
    public class GetItemRelatedSellerRequest
    {
        public string ItemCode { get; set; }
        public string ItemColor { get; set; }
        public string PrintMethod { get; set; }
        public int NumberOfColors { get; set; }
        public bool ExcludeClearance { get; set; }
        public int ItemLimit { get; set; }
    }
}
