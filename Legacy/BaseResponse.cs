﻿using System;
using System.Linq;

namespace bel.core.Legacy
{
    public class BaseResponse
    {
        public bool Error { get; set; }

        public int ErrorCount { get; set; }

        public int StatusCode { get; set; }

        public string[] ErrorMessages { get; set; }

        public BaseResponse()
        {
            Error = false;

            ErrorCount = 0;
        }

        public BaseResponse(Exception ex)
        {
            Error = true;

            ErrorCount = 1;

            ErrorMessages = new[] { ex.Message };
        }

        public BaseResponse(string[] ErrorMessage)
        {
            Error = true;

            ErrorCount = ErrorMessage.Count();

            ErrorMessages = ErrorMessage;
        }

        public void SetError(Exception exception)
        {
            Error = true;

            ErrorCount = 1;

            ErrorMessages = new[] { exception.Message };
        }

        public void SetError(int ErrorCount, Exception exception)
        {
            Error = true;

            this.ErrorCount = 1;

            ErrorMessages = new[] { exception.Message };
        }
    }
}
